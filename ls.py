import threading
import sys
import socket

def server():


    ls_port = int(sys.argv[1])
    ts1_hostname = sys.argv[2]
    ts1_port = int(sys.argv[3])
    ts2_hostname = sys.argv[4]
    ts2_port = int(sys.argv[5])

    try:
        ss = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  #ss = server socket
        ts1 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        ts2 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        print("[S]: Server sockets created")
    except socket.error as err:
        print('socket open error: {}\n'.format(err))
        exit()

    #Establish connection with client
    server_binding = ('', ls_port)   
    ss.bind(server_binding)
    ss.listen(1)
    host = socket.gethostname()  
    localhost_ip = (socket.gethostbyname(host))
    csockid, addr = ss.accept()   

    #Connect to ts1
    ts1_address = socket.gethostbyname(ts1_hostname) 
    server_binding = (ts1_address, ts1_port)
    ts1.connect(server_binding)
    ts1.setblocking(0)
    ts1.settimeout(5)

    ts2_address = socket.gethostbyname(ts2_hostname) 
    server_binding = (ts2_address, ts2_port)
    ts2.connect(server_binding)
    ts2.setblocking(0)
    ts2.settimeout(5)
    #Keep server open until connections are done


    data_from_client = 0
    ts1_timeout = False
    ts2_timeout = False
    while True:

        #Recieve message from client. 
        #If client connection is closed with no data recieved, print exception 
        data_from_client = csockid.recv(1024)
      
        if not data_from_client:
            print("[S]: Client connection closed")
            exit()

        query = data_from_client.decode('utf-8')

        ts1.send(query.encode('utf-8'))

        try:
             data_from_server = ts1.recv(1024)
        except socket.timeout:
            ts1_timeout = True

        if(ts1_timeout):

            ts2.send(query.encode('utf-8'))
            try:
                data_from_server = ts2.recv(1024)
            except socket.timeout:
                ts2_timeout = True
                print("ERROR TS2")

        if(ts2_timeout):
            response = query + " - Error:HOST NOT FOUND\n"
        else:
            response = data_from_server.decode('utf-8')

        csockid.send(response.encode('utf-8'))
        ts1_timeout = False ; ts2_timeout = False




if __name__ == "__main__":
    t1 = threading.Thread(name='server', target=server)
    t1.start()

    print("Done.")






