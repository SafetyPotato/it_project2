import threading
import sys
import socket

def client():

    #Pull command line arguments
    ls_hostname = sys.argv[1]
    ls_port = int(sys.argv[2])
     
    try:
        ls = socket.socket(socket.AF_INET, socket.SOCK_STREAM) #cs = client socket
        print("[C]: Client socket to rs created")
    except socket.error as err:
        print('socket open error: {} \n'.format(err))
        exit()
    
    #Bind server and host and connect
    ls_address = socket.gethostbyname(ls_hostname) 
    server_binding = (ls_address, ls_port)
    ls.connect(server_binding)

    fr = open("PROJ2-HNS.txt", "r")
    fw = open("RESOLVED.txt", "w")
    lines = fr.read().splitlines()

    for name in lines:
        #Send and recieve data
        ls.send(name.encode('utf-8'))
        data_from_server = ls.recv(1024)
        response = data_from_server.decode('utf-8')
        fw.write(response)

    #Close files and socket connections
    fr.close() ; fw.close()
    ls.close()

    print("[C]: rs and ts connections closed")
    exit()



if __name__ == "__main__":
    t2 = threading.Thread(name='client', target=client)
    t2.start()

    print("Done.")
