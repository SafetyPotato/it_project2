0. Thomas Nardello, tan89; Jilvia D'Souza, jmd664

1.  We implemented our LS level functionality by first querying ts1 to see if there was a response. We set timers for both t1 and t2 to be 5 seconds. If t1 does not respond in 5 seconds, we set a boolean so the program knows we need to attempt to query t2. If t2 times out as well, we send the client an error message. If either t1 or t2 responds, we send the message back to the client.

2. Not that we know of.

3. We did not encounter any problems worth noting when working on the project. 

4. Working on this project gave us a greater understanding of how load-balancing servers work. As the name implies, their purpose is to shift the load of querying other DNS servers away from DNS servers that have domain name tables, so that they can spend their time responding to queries instead. A very simplied version of this functionality is shown in our program. 
