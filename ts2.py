import threading
import socket
import sys

def server():
    try:
        ss = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  #ss = server socket
        print("[S]: Server socket created")
    except socket.error as err:
        print('socket open error: {}\n'.format(err))
        exit()

    port = int(sys.argv[1])
    server_binding = ('', port)   
    ss.bind(server_binding)
    ss.listen(1)
    host = socket.gethostname()  
    localhost_ip = (socket.gethostbyname(host))
    csockid, addr = ss.accept()   

    #Read data from file into dns table
    with open("PROJ2-DNSTS2.txt", "r") as f:
        dns_table = []
        for line in f:
            line = line.replace('\r', '')
            dns_table.append(line)

    #Keep server open until connections are done
    while True:

        #Recieve message from client. 
        #If client connection is closed with no data recieved, print exception
        data_from_client = csockid.recv(1024)

        if not data_from_client:
            print("[S]: Client connection closed")
            exit()

        query = data_from_client.decode('utf-8')
        matchFound = False

        for entry in dns_table:
                
            #Get first word of line
            hostname = entry.split(" ")[0] 

            #Check for case insensitive match
            if hostname.lower() == query.lower():
                matchFound = True
                response = entry

        #send back to client
        if(matchFound): csockid.send(response.encode('utf-8'))


       



if __name__ == "__main__":
    t1 = threading.Thread(name='server', target=server)
    t1.start()

    print("Done.")






